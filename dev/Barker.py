#!/usr/bin/env python



import os
import sys
import numpy







class Barker(object):

    # ----------------------------------------------------------------------- #

    __LENGTHS = [2,3,4,5,7,11,13]

    # ----------------------------------------------------------------------- #

    def __str__(self):
        info  = "Barker-Code:\n"
        info += "-----------:\n"
        return info

    # ----------------------------------------------------------------------- #

    def __init__(self,length=2):
        """
        """
        self.set_length(length)

    # ----------------------------------------------------------------------- #

    def get_LENGTHS(self):
        return self.__LENGTHS
    LENGTHS = property(get_LENGTHS)

    # ----------------------------------------------------------------------- #

    def get_length(self):
        return self.__length
    def set_length(self,value):
        if not isinstance(value,int):
            raise TypeError()
        if not value in self.__LENGTHS:
            raise ValueError()
        self.__length = value
        try:
            self.__set__code()
        except AttributeError:
            pass
    length = property(get_length,set_length)

    # ----------------------------------------------------------------------- #

    def get_code(self):
        return self.__code
    def __set__code(self):
        length = self.__length
        if length==2 : code = [1.0]*length
        if length==3 : code = [1.0]*length
        if length==4 : code = [1.0]*length
        if length==5 : code = [1.0]*length
        if length==7 : code = [1.0]*length
        if length==11: code = [1.0]*length
        if length==13: code = [1.0]*length
        code = numpy.asarray(code)
        code = numpy.complex64(code)
        self.__code = code
    code = property(get_code)

    # ----------------------------------------------------------------------- #

    


class BinaryPhase(object):

    def __init__(self,seed=0,length=1):
        self.set_seed(seed)
        self.set_length(length)


    def get_seed(self):
        return self.__seed
    def set_seed(self,value):
        if not isinstance(value,int):
            raise TypeError()
        self.__seed = value
        try:
            self.__set__code()
        except AttributeError:
            pass
    seed = property(get_seed,set_seed)


    def get_length(self):
        return self.__length
    def set_length(self,value):
        if not isinstance(value,int):
            raise TypeError()
        if value <= 0:
            raise ValueError()
        self.__length = value
        try:
            self.__set__code()
        except AttributeError:
            pass
    length = property(get_length,set_length)


    def get_code(self):
        return self.__code
    def __set__code(self):
        numpy.random.seed(self.__seed)
        code = numpy.random.random(self.__length)
        code = numpy.exp(2.0*numpy.pi*1.0j*code)
        code = numpy.angle(code)
        code = -1.0*numpy.sign(code)
        code = code.real
        code = numpy.complex64(code)
        self.__code = code
    code = property(get_code)



class Complementary(object):

    __LENGTHS = [2,4,8,16,32]

    def __init__(self): pass

    def get_LENGTHS(self):
        return self.__LENGTHS
    LENGTHS = property(get_LENGTHS)

    def get_length(self): return self.__length
    def set_length(self): pass
    length = property(get_length,set_length)

    def get_code(self): return self.__code
    def set_code(self): pass


















if __name__ == "__main__":

    bp = BinaryPhase(seed=7,length=10)
    print bp.code


    sys.exit()